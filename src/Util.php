<?php

namespace Mkjp\ExtensionMethods;


final class Util {
    /**
     * Checks if $unit is derived from $from in some way
     * 
     *   - If $unit is an interface, then it checks that $from is also an interface
     *     and $unit (or one of its ancestors) extends $from
     * 
     *   - If $unit is a trait, it checks that $from is also a trait and that $unit
     *     (or one of its ancestors) uses $from
     * 
     *   - If $unit is a class and $from is an interface, it checks that $unit
     *     (or one of its ancestors) implements $from
     * 
     *   - If $unit is a class and $from is a trait, it checks that $unit (or one
     *     of its ancestors) uses $from
     * 
     *   - If $unit and $from are both classes, it checks that $unit extends $from
     * 
     * If $unit and $from are not \ReflectionClass objects, they are converted
     * 
     * @param mixed $unit
     * @param mixed $from
     * @return bool
     */
    public static function isDerivedFrom($unit, $from) {
        if( !($unit instanceof \ReflectionClass) )
            $unit = new \ReflectionClass($unit);
        if( !($from instanceof \ReflectionClass) )
            $from = new \ReflectionClass($from);
            
        if( $unit->isInterface() ) {
            // Check if $unit implements $from
            return $from->isInterface() && $unit->implementsInterface($from->getName());
        }
        else if( $unit->isTrait() ) {
            // If $from is not a trait, $unit can't possibly derive from it
            if( !$from->isTrait() ) return false;
            
            // If they are the same trait, then return true
            if( $unit->getName() === $from->getName() ) return true;
            
            // Otherwise check if any of the used traits are derived from the given trait
            foreach( $unit->getTraits() as $trait ) {
                if( self::isDerivedFrom($trait, $from) ) return true;
            }
            
            return false;
        }
        else {
            // $unit is a class
            
            if( $from->isInterface() ) {
                // Check if it implements $from
                return $unit->implementsInterface($from->getName());
            }
            else if( $from->isTrait() ) {
                // Check if any of the used traits are derived from $from
                foreach( $unit->getTraits() as $trait ) {
                    if( self::isDerivedFrom($trait, $from) ) return true;
                }
                
                // If none of the directly used traits pass, then check if the
                // parent class is derived from $from
                $parent = $unit->getParentClass();
                return ( $parent !== false && static::isDerivedFrom($parent, $from) );
            }
            else {
                // If $from is also a class, check if $unit is a subclass
                return $unit->getName() === $from->getName() ||
                       $unit->isSubclassOf($from->getName());
            }
        }
    }
    
    /**
     * Checks if the given programming unit (i.e. interface, trait or class) is
     * extension method aware
     * 
     * $unit can be a string interface/trait/class name, a \ReflectionClass
     * or any other object
     * 
     *   - An interface is extension method aware if it (or one of its ancestors)
     *     extends ExtensionMethodAware
     * 
     *   - A trait or class is extension method aware if it (or one of its ancestors)
     *     uses ExtensionMethodAwareTrait
     * 
     * Note that we require classes to have used the trait at some point in the
     * inheritance hierarchy, not just implement the ExtensionMethodAware interface
     * 
     * This is to ensure (as much as possible) that they have picked up the correct
     * implementation of __call at some point
     * 
     * @param mixed $unit
     * @return bool
     */
    public static function isExtensionMethodAware($unit) {
        if( !($unit instanceof \ReflectionClass) )
            $unit = new \ReflectionClass($unit);
        
        // Only if the class is an interface do we consider the ExtensionMethodAware
        // interface sufficient
        if( $unit->isInterface() )
            return static::isDerivedFrom($unit, ExtensionMethodAware::class);
        
        // Otherwise, check if it is derived from ExtensionMethodAwareTrait
        return static::isDerivedFrom($unit, ExtensionMethodAwareTrait::class);
    }
}
