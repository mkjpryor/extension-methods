<?php

namespace Mkjp\ExtensionMethods;


/**
 * Marker interface to indicate that an object is extension method aware, and forces
 * implementors to implement __call
 * 
 * This interface should be implemented using the ExtensionMethodAwareTrait
 */
interface ExtensionMethodAware {
    /**
     * Called when a missing method is encountered, and attempts to locate a suitable
     * extension method
     * 
     * @param string $method  The called method name
     * @param array  $args    The arguments the method was called with
     */
    public function __call($method, array $args);
}
