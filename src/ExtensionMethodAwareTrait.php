<?php

namespace Mkjp\ExtensionMethods;


/**
 * Trait that should be used to implement the ExtensionMethodAware interface, or
 * can be used independently of the interface to enable extension method awareness
 */
trait ExtensionMethodAwareTrait {
    /**
     * A cache of previously resolved methods
     */
    private $cache = [];
    
    /**
     * Called when a missing method is encountered, and attempts to locate a suitable
     * extension method
     * 
     * @param string $methodName  The called method name
     * @param array  $args        The arguments the method was called with
     * @return mixed
     * 
     * @throws \BadMethodCallException
     */
    public function __call($methodName, array $args) {
        if( isset($this->cache[$methodName]) ) {
            // If we already have a cached method, use it
            $method = $this->cache[$methodName];
        }
        else {
            // Otherwise, try to resolve it in the registry
            $method = Registry::resolve(get_called_class(), $methodName);
        
            if( $method === null )
                throw new \BadMethodCallException(
                    "Call to undefined method " . get_called_class() . "::$methodName()"
                );
                
            // Create a new instance of the closure that is bound to $this
            $method = $method->bindTo($this, $this);
                
            // Cache the resolved method for next time
            $this->cache[$methodName] = $method;
        }
        
        // Just pass the arguments on
        // Remember, $this is already bound to this object in the closure
        return $method(...$args);
    }
}
