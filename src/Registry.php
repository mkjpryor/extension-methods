<?php

namespace Mkjp\ExtensionMethods;


/**
 * This class provides a registry for extension method implementations
 */
final class Registry {
    /**
     * All the available extension methods, indexed by method name then by
     * interface/trait/class name
     */
    private static $methods = [];
    
    /**
     * Registers an extension method with the given name for the given
     * interface/trait/class
     * 
     * The given implementation will have $this bound correctly and will be executed
     * in the correct class scope
     * 
     * @param string   $receiver  The interface/trait/class receiving the extension method
     * @param string   $method    The name of the extension method
     * @param \Closure $impl      The implementation of the method
     * @return void
     */
    public static function register($receiver, $method, \Closure $impl) {
        // Check that the receiver is extension method aware, as if it not the
        // extension methods will not be used
        if( !Util::isExtensionMethodAware($receiver) )
            throw new \InvalidArgumentException(
                "Cannot attach extension methods to an interface/trait/class that " .
                "is not extension method aware"
            );
        
        // Just register the method for later
        if( !isset(static::$methods[$method]) ) static::$methods[$method] = [];
        static::$methods[$method][$receiver] = $impl;
    }
    
    /**
     * Given a map of methodName => impl, registers each element of the map as
     * an extension method for the given interface/trait/class
     * 
     * @param string $receiver  The interface/trait/class receiving the extension methods
     * @param array  $methods   Map of methodName => implementation
     * @return void
     */
    public static function registerAll($receiver, array $methods) {
        foreach( $methods as $name => $impl ) static::register($receiver, $name, $impl);
    }
    
    /**
     * Tries to resolve an extension method with the given name for the given
     * interface/trait/class somewhere in its heirarchy
     * 
     * If an extension method is found, a callable is returned that can be used
     * to execute it
     * 
     * If no extension method is found, null is returned
     * 
     * @param string $receiver    The interface/trait/class to resolve the method for
     * @param string $methodName  The method name to attempt to resolve
     * @return \Closure|null
     */
    public static function resolve($receiver, $methodName) {
        // Start by seeing if any methods have been registered with the given name
        if( !isset(static::$methods[$methodName]) ) return null;
        
        /**
         * Then see if the receiver class is compatible with any of the implementations
         *
         * If the receiver is compatible with more than one implementation, we want
         * to use the most specific
         *
         * I.e. we want to be able to resolve this scenario correctly
         * 
         *     class Test { use ExtensionMethodAwareTrait; }
         *     class Test2 extends Test { }
         * 
         *     Registry::register('Test', 'myMethod', function($self) { ... });
         *     Registry::register('Test2', 'myMethod', function($self) { ... });
         * 
         *     $t = new Test();
         *     $t->myMethod(); // Uses first implementation
         *     $t2 = new Test2();
         *     $t2->myMethod(); // Uses second implementation
         */
        $method = null;
        $foundClass = null;
        foreach( static::$methods[$methodName] as $class => $impl ) {
            // First, check if the class is a match for the receiver
            if( Util::isDerivedFrom($receiver, $class) ) {
                // Check if this is a more specific implementation than any we have
                // already found
                if( $foundClass === null || Util::isDerivedFrom($class, $foundClass) ) {
                    $foundClass = $class;
                    $method = $impl;
                }
            }
        }
        return $method;
    }
}
